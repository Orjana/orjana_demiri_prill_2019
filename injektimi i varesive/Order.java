package classes;

public class Order 
{
	private Customer client;
	private String productName;
	private int price;
	private String orderDate;
	
	
	
	public Customer getClient() {
		return this.client;
	}



	public void setClient(Customer client) {
		this.client = client;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public int getPrice() {
		return price;
	}



	public void setPrice(int price) {
		this.price = price;
	}



	public String getOrderDate() {
		return orderDate;
	}



	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}



	public Order(Customer client, String productName, int price, String orderDate) 
	{
		super();
		this.client = client;
		this.productName = productName;
		this.price = price;
		this.orderDate = orderDate;
	}
	
	

}
