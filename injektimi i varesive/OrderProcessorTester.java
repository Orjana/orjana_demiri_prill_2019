package test;

import org.picocontainer.*;

import classes.CreditCard;
import classes.Customer;
import classes.Order;
import classes.OrderProcesor;
import classes.PaymentGateway;
import classes.PaymentGateWayStub;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.*;

class OrderProcessorTester {

	@Before
	private MutablePicoContainer configureContainer()
	{
		MutablePicoContainer pico = new DefaultPicoContainer();
		CreditCard credit = new CreditCard("1","Aster Danaj",500);
		Customer customer = new Customer(credit.getCardHolder(),credit);
		Order order = new Order(customer,"Coca Cola",150,"2018-12-13");
	    pico.addComponent(credit);
		pico.addComponent(customer);
		pico.addComponent(order);
	    pico.addComponent(PaymentGateWayStub.class);
		
		return pico;
	}
	
	
	@Test
	public void testWithPico()
	{
		MutablePicoContainer pico = configureContainer();
		Order order = pico.getComponent(Order.class);
		CreditCard creditCard = pico.getComponent(CreditCard.class);
		PaymentGateWayStub paymentGateway = pico.getComponent(PaymentGateWayStub.class);
		OrderProcesor orderProcesor = new OrderProcesor(order,paymentGateway);
	    assertEquals("Transaksioni u krye me sukses!",orderProcesor.process());		
	}
	

}
