package classes;

public class PaymentGateway 
{
    protected CreditCard creditCard;    
    
	public CreditCard getCreditCard() {
		return creditCard;
	}


	public void setCreditCard(CreditCard creditCard) 
	{
		this.creditCard = creditCard;
	}


	public PaymentGateway() 
	{
		super();
	}
	
	public PaymentGateway(CreditCard creditCard) {
		super();
		this.creditCard = creditCard;
	}


	public int procces(Order order)
	{  
		
		if(this.creditCard.reduceAmount(order.getPrice()))
		{
		
		    return this.creditCard.getAmount();
		
		}
		else
		{
			return -1;
		}
	}
	
	

}
