package classes;

public class OrderProcesor 
{
	private Order order;
	private PaymentGateway gateway;
		
	public Order getOrder() 
	{
		return order;
	}

	public void setOrder(Order order) 
	{
		this.order = order;
	}
	
	public OrderProcesor(Order order)
	{
		super();
		this.order = order;
	}

	public OrderProcesor(Order order, PaymentGateway gateway)
	{
		super();
		this.order = order;
		this.gateway = gateway;
	}

	public String process()
	{
		int currentAmount = this.order.getClient().getCredit().getAmount();
		int result;
		String output;
		this.gateway.setCreditCard(this.order.getClient().getCredit());
		result = gateway.procces(order);
		if (currentAmount > result)
		{
			output = "Transaksioni u krye me sukses!";
		}
		else
		{
			output = "Transaksioni deshtoi!";
		}
			
		
		return output;
	}
}
