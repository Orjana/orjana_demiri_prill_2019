package classes;

public class Customer
{
	private String fullName;
	private CreditCard credit;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public CreditCard getCredit() {
		return credit;
	}
	public void setCredit(CreditCard credit) {
		this.credit = credit;
	}
	public Customer(String fullName, CreditCard credit) {
		super();
		this.fullName = fullName;
		this.credit = credit;
	}
	 
	

}
