package classes;

public class CreditCard 
{
	private String cardnr;
	private String CardHolder;
	private int Amount;
	
	
	
	public String getCardnr() {
		return cardnr;
	}



	public void setCardnr(String cardnr) {
		this.cardnr = cardnr;
	}



	public String getCardHolder() {
		return CardHolder;
	}



	public void setCardHolder(String cardHolder) {
		CardHolder = cardHolder;
	}



	public int getAmount() {
		return Amount;
	}



	public void setAmount(int amount) {
		Amount = amount;
	}



	public CreditCard(String cardnr, String cardHolder, int amount) {
		super();
		this.cardnr = cardnr;
		CardHolder = cardHolder;
		Amount = amount;
	}
	
	
	
	public CreditCard(String cardHolder) {
		super();
		CardHolder = cardHolder;
	}



	public CreditCard() {
		super();
	}



	public boolean reduceAmount(int amount) 
	{
		
		boolean flag=false;
		
		if (this.Amount < amount)
		{
			flag = false;
		}
		else
		{
			this.Amount -= amount;
			flag = true;
		}
		
		return flag;
		
	}
}
