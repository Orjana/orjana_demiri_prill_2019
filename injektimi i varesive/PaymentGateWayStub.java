package classes;

public class PaymentGateWayStub extends PaymentGateway 
{
	 @Override
	 public void setCreditCard(CreditCard creditCard) 
     {
		 this.creditCard = creditCard;
	 }
	 
     @Override 
     public int procces(Order order)
     {
    	 return 200;
     }
}
